#include <iostream>
#include <string>
#include "stack.h"

using namespace std;

int getResult(std::istream& stream)
{

    int num1=0,num2=0,num=0,i=0;
    stack abacus;
    bool isNum = false;
    //istreambuf_iterator<char> eos;
    //string str(std::istreambuf_iterator<char>(stream), eos);

    string str;
    stream >> str;

   //for (unsigned int i = 0; i < str.length(); i++){
     while (true){
        //string str;  string gets cut seemingly - no idea why - stol? - string doesnt get cut, every char is being interpreted as a string.
        //cin >> str; seems to work only with cin, not with getline.

        if(str[i] == '+'){
            num2 = abacus.pop();
            num1 = abacus.pop();
            abacus.push(num1+num2);
            //i++;
        }
        else if(str[i] == '-'){
            num2 = abacus.pop();
            num1 = abacus.pop();
            abacus.push(num1-num2);
            //i++;
        }
        else if(str[i] == '*'){
            num2 = abacus.pop();
            num1 = abacus.pop();
            abacus.push(num1*num2);
            //i++;
        }
        else if(str[i] == '/'){
            num2 = abacus.pop();
            num1 = abacus.pop();
            abacus.push(num1/num2);
            //i++;
        }
        else if(str[i] == '%'){
            num2 = abacus.pop();
            num1 = abacus.pop();
            abacus.push(num1%num2);
            //i++;
        }
        else if(str[i] == '='){
            num1 = abacus.pop();
            return num1;
        }
        else if(str[i] == ' '){
                if(isNum == true){
                    abacus.push(num);
                    isNum = false;

                }
        }
        else {
            if (isNum == false){
                abacus.push(str[i]);
                isNum = true;
            }
            else{
                num2 = abacus.pop();
                num = num2*10 + str[i];
                isNum = true;
            }
        }
        i++;
    }
}
/*
void stack::push(double i_val){
    node *n = new node;
    n->val = i_val;
    n->next = head;
    head = n; //??? nicht sehr einleuchtend...
}

double stack::pop(){
        node *n = head;
        double i_val = n->val;
        head = head->next;
        delete n;
        return i_val;
}
*/
