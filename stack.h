#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED

#include <iostream>

int getResult(std::istream& stream);

class node{
    public:    //things that can be seen by everyone.
        int val = 0;
        node *next = nullptr;
};

class stack{
    private:   //things that only the programmer needs to know.
        struct node *head;

    public:
        void push(int val);
        int pop();

        //constructor for the stack.
        stack(){
            head = nullptr;
        }
};

#endif // STACK_H_INCLUDED
